DROP TABLE IF EXISTS notes;

CREATE TABLE notes (
  id int NOT NULL PRIMARY KEY,
  text varchar(50) NOT NULL);