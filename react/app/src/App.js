import React, { useState } from "react";
import axios from "axios";
import "./App.sass";

function Todo({ todo, index, sendTodo, removeTodo }) {
  return (
    <div className="todo"> 
      
      {todo.text}

    <div>
        <button onClick={() => sendTodo(index)}>send</button>
        <button onClick={() => removeTodo(index)}>x</button>
      </div>
    </div>
  );
}

function TodoForm({ addTodo }) {
  const [value, setValue] = useState("");

  const handleSubmit = e => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        className="input"
        value={value}
        onChange={e => setValue(e.target.value)}
      />
    </form>
  );
}

function App() {
  const [todos, setTodos] = useState([]);

  const addTodo = text => {
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };

  const sendTodo = index => {
    const newTodos = [...todos];
    const note = {
      id: index,
      text: newTodos[index].text
    };

 axios.post('http://localhost:800', note)
    .then(function(response){
      console.log(response);
    })
    .catch(function(error){
      console.log(error);
    });
  };

  const removeTodo = index => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };

  return (
    <div className="app">
      <div className="todo-list">
        {todos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            todo={todo}
            sendTodo={sendTodo}
            removeTodo={removeTodo}
          />
        ))}
        <TodoForm addTodo={addTodo} />
      </div>
    </div>
  );
}

export default App;